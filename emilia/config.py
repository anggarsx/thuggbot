#if not __name__.endswith("sample_config"):
#    import sys
#    print("The README is there to be read. Extend this sample config to a config file, don't just rename and change "
#          "values here. Doing that WILL backfire on you.\nBot quitting.", file=sys.stderr)
#    quit(1)


# Create a new config.py file in same dir and import, then extend this class.
class Config(object):
    LOGGER = True

    # REQUIRED
    API_KEY = "794864310:AAFV6F5_E4BkxHqv0NLiETlDp_OneDN4pPY"
    OWNER_ID = "424935819"  # If you dont know, run the bot and do /id in your private chat with it
    OWNER_USERNAME = "ARSLevantein"
    # Some API is required for more features
    API_OPENWEATHER = "e9e7a5654659de6d71cb6317ec633834"
    API_ACCUWEATHER = "i4qzVJXHErBXe9vxuG2ccRb7A7y9SLjA"
    MAPS_API = "AIzaSyBsB5p2k_O1XP0Of4bg8RTITOAM1Tsb5sg"
    CAT_API_KEY = None
    DOG_API_KEY = None

    # RECOMMENDED
    #ThuggBot    SQLALCHEMY_DATABASE_URI = 'postgres://warmxkjujnfjmy:c5fa46d5dca75799bf77c542d20a79da637e5a6725266ffb1e8d1fa44aaead8d@ec2-79-125-4-72.eu-west-1.compute.amazonaws.com:5432/d4ka9nit7du10u'
    #SQLALCHEMY_DATABASE_URI = 'postgres://xuxzvmpccvqatt:26d7abf2073bbbb8ee5bfe55856a281217c045e9f9719b476ae0a43c8c9e0211@ec2-107-21-126-201.compute-1.amazonaws.com:5432/d19lmkphhh9nl7'  # needed for any database modules
    # NOW SQLALCHEMY_DATABASE_URI = 'postgres://ahbnliodjehivk:639ea238486dafae410543aec62bfd31f7d632a20630dc2466240e4e6fa83330@ec2-54-221-215-228.compute-1.amazonaws.com:5432/dandb4hg3anrj8'
    # SQLALCHEMY_DATABASE_URI = 'postgres://gejkwisjxthlof:ebb7f043431353f039f8dde03e93ab8ce89e3ed96ab619febc3153a6c16d6b9b@ec2-184-72-238-22.compute-1.amazonaws.com:5432/d3sbpqq4l1ltfn'
    # SQLALCHEMY_DATABASE_URI = 'postgres://wdltnbjudjtuep:f9cdfec72b6ef8c3a7e7e04c66e3d734c7930b3d27f5527ac1b9bed7bbe498ad@ec2-54-247-170-5.eu-west-1.compute.amazonaws.com:5432/d8kougvskvlp8q'
    # SQLALCHEMY_DATABASE_URI = 'postgres://zsuceynxysphzu:57a75af7c212924b67bdf4b2d01fff3c9156e7aec53ea2642aeeb1c1a4aa6ba4@ec2-174-129-218-200.compute-1.amazonaws.com:5432/df3hin0boqf77i'
    SQLALCHEMY_DATABASE_URI = 'postgres://imxkpmdabatzen:be4330873b246bcbf0f81fa568022f408014cbd10ceb727ec48c5ceba48fa875@ec2-23-21-249-0.compute-1.amazonaws.com:5432/dcida06lp4pouf'
    MESSAGE_DUMP = None  # needed to make sure 'save from' messages persist
    LOAD = []
    NO_LOAD = ['translation', 'weather', 'devs']
    WEBHOOK = False
    URL = None

    # OPTIONAL
    SUDO_USERS = [424935819, 741331964]  # List of id's (not usernames) for users which have sudo access to the bot.
    SUPPORT_USERS = [424935819]  # List of id's (not usernames) for users which are allowed to gban, but can also be banned.
    WHITELIST_USERS = [424935819]  # List of id's (not usernames) for users which WONT be banned/kicked by the bot.
    DONATION_LINK = 'https://www.paypal.me/anggaxyz'  # EG, paypal
    CERT_PATH = None
    PORT = 5000
    DEL_CMDS = False  # Whether or not you should delete "blue text must click" commands
    STRICT_GBAN = False
    WORKERS = 8  # Number of subthreads to use. This is the recommended amount - see for yourself what works best!
    BAN_STICKER = 'CAADAgADOwADPPEcAXkko5EB3YGYAg'  # banhammer marie sticker
    ALLOW_EXCL = False  # Allow ! commands as well as /
    SPAMMERS = "" # Will not allow to interact with bot
    TEMPORARY_DATA = None # Temporary data for backup module, use int number
    DEEPFRY_TOKEN = None

class Production(Config):
    LOGGER = False


class Development(Config):
    LOGGER = True
